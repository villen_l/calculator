/*
** EPITECH PROJECT, 2020
** Calculator
** File description:
** Created by Lucas Villeneuve,
*/

#include <iostream>
#include <stdexcept>
#include "Engine.hh"

std::string Engine::insertNumber(double nb)
{
	if (_currentOp != Operation::RESULT)
	{
		_result.append(doubleToString(nb));
		_currentNb = _currentNb * 10 + nb;
	}
	else
	{
		_result = doubleToString(nb);
		_currentNb = nb;
		_currentOp = Operation::NOTHING;
	}
	return _result;
}

void	Engine::initOperation()
{
	if (isArithmeticOperation())
	{
		calculateResult();
	}
	else
	{
		checkIfResultEmpty();
	}
}

std::string Engine::add()
{
	try {
		initOperation();
	} catch (std::runtime_error &e)
	{
		clear();
		return e.what();
	}

	_result.append(" + ");
	_currentOp = Operation::ADD;
	storeOldNb();
	return _result;
}

std::string Engine::subtract()
{
	try {
		initOperation();
	} catch (std::runtime_error &e)
	{
		clear();
		return e.what();
	}
	checkIfResultEmpty();
	_result.append(" - ");
	_currentOp = Operation::SUB;
	storeOldNb();
	return _result;
}

std::string Engine::multiply()
{
	try {
		initOperation();
	} catch (std::runtime_error &e)
	{
		clear();
		return e.what();
	}

	checkIfResultEmpty();
	_result.append(" x ");
	_currentOp = Operation::MUL;
	storeOldNb();
	return _result;
}

std::string Engine::divide()
{
	try {
		initOperation();
	} catch (std::runtime_error &e)
	{
		clear();
		return e.what();
	}

	checkIfResultEmpty();
	_result.append(" / ");
	_currentOp = Operation::DIV;
	storeOldNb();
	return _result;
}

std::string Engine::doubleToString(double nb) const
{
	std::string str = std::to_string(nb);

	if (str.find('.') != std::string::npos)
	{
		// Remove trailing zeros and dots
		str.erase(str.find_last_not_of('0') + 1, std::string::npos);
		str.erase(str.find_last_not_of('.') + 1, std::string::npos);
	}

	return str;
}

double Engine::calculate(double nb1, double nb2, Engine::Operation op)
{
	switch (op)
	{
	case Operation::ADD:
		return nb1 + nb2;
	case Operation::SUB:
		return nb1 - nb2;
	case Operation::MUL:
		return nb1 * nb2;
	case Operation::DIV:
		if (nb2 == 0.0)
			throw std::runtime_error(ErrorMsgDivideBy0);
		return nb1 / nb2;
	default:
		break;
	}
	return 0;
}

std::string Engine::calculateResult()
{
	double tmp = calculate(_oldNb, _currentNb, _currentOp);
	this->clear();
	_currentOp = Operation::RESULT;
	_currentNb = tmp;
	_result = doubleToString(tmp);
	return _result;
}

std::string Engine::getResult()
{
	try {
		return calculateResult();
	} catch (const std::runtime_error &e)
	{
		this->clear();
		return e.what();
	}

}

void Engine::storeOldNb()
{
	_oldNb = _currentNb;
	_currentNb = 0;
}

std::string Engine::clear()
{
	_oldNb = 0;
	_currentNb = 0;
	_result.clear();
	_currentOp = Operation::NOTHING;
	return "0";
}

void Engine::checkIfResultEmpty()
{
	if (_result.empty())
	{
		_result = "0";
	}
}

bool Engine::isArithmeticOperation()
{
	return _currentOp == Operation::ADD ||
		_currentOp == Operation::SUB ||
		_currentOp == Operation::DIV ||
		_currentOp == Operation::MUL;
}
