/*
** EPITECH PROJECT, 2020
** Calculator
** File description:
** Created by Lucas Villeneuve,
*/

#ifndef CALCULATOR_ENGINE_HH
#define CALCULATOR_ENGINE_HH

#include <string>

/** TODO
 *  - CE
 *  - Dot button
 *  - Backsoace button
 */

class Engine
{
	public:
		Engine() = default;
		~Engine() = default;

		Engine(const Engine &) = default;
		Engine &operator=(const Engine &) = default;

		std::string	insertNumber(double nb);
		std::string	add();
		std::string	getResult();
		std::string 	subtract();
		std::string 	multiply();
		std::string 	divide();
		std::string	clear();

		// Utility
		std::string doubleToString(double nb) const;

	private:
		enum class Operation
		{
			NOTHING = 0,
			ADD,
			SUB,
			MUL,
			DIV,
			RESULT
		};

		double	calculate(double nb1, double n2, Operation op);
		std::string calculateResult();
		void 	storeOldNb();
		void	initOperation();

		// Set _result to "0" if it is empty
		void	checkIfResultEmpty();

		bool	isArithmeticOperation();

		double _oldNb = 0.0f;
		double _currentNb = 0.0f;
		Operation _currentOp = Operation::NOTHING;
		std::string _result;

		const std::string ErrorMsgDivideBy0 = "Cannot divide by 0.";
};

#endif //CALCULATOR_ENGINE_HH
