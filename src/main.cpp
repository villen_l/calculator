#include <iostream>
#include "ViewContainer.hh"
#include "ListLayoutModel.hh"
#include "TextViewModel.hh"
#include "GridLayoutModel.hh"
#include "ButtonModel.hh"
#include "Engine.hh"

int main(int argc, char **argv)
{
	Engine engine;

	ViewContainer viewContainer(argc, argv);
	viewContainer.confWindow(350, 550, "Calculator");

	auto listLayoutModel = viewContainer.createModel<ListLayoutModel>("listLayout");
	listLayoutModel->guiBaseOptions.width = 350;
	listLayoutModel->guiBaseOptions.height = 550;

	auto result = listLayoutModel->addChild<TextViewModel>("TextResult");
	result->guiBaseOptions.width = 300;
	result->guiBaseOptions.height = 100;
	result->guiTextOptions.fontSize = 12;
	result->guiBaseOptions.marginLeft = 25;
	result->guiBaseOptions.marginTop = 25;
	result->guiTextOptions.font = "Verdana";
	result->text = "Result = 0";

	// Buttons
	auto buttonGrid = listLayoutModel->addChild<GridLayoutModel>("buttonGrid");
	buttonGrid->guiBaseOptions.width = 300;
	buttonGrid->guiBaseOptions.height = 375;
	buttonGrid->guiBaseOptions.marginLeft = 25;
	buttonGrid->guiBaseOptions.marginRight = 25;
	buttonGrid->guiBaseOptions.marginTop = 25;

	/* Row 1 */
	auto b_clear = buttonGrid->addChild<ButtonModel>("clear", IGridLayout::GridPos{75, 0, 75, 75});
	auto b_div = buttonGrid->addChild<ButtonModel>("div", IGridLayout::GridPos{225, 0, 75, 75});

	/* Row 2 */
	auto b_seven = buttonGrid->addChild<ButtonModel>("7", IGridLayout::GridPos{0, 75, 75, 75});
	auto b_eight = buttonGrid->addChild<ButtonModel>("8", IGridLayout::GridPos{75, 75, 75, 75});
	auto b_nine = buttonGrid->addChild<ButtonModel>("9", IGridLayout::GridPos{150, 75, 75, 75});
	auto b_mul = buttonGrid->addChild<ButtonModel>("mul", IGridLayout::GridPos{225, 75, 75, 75});

	/* Row 3 */
	auto b_four = buttonGrid->addChild<ButtonModel>("4", IGridLayout::GridPos{0, 150, 75, 75});
	auto b_five = buttonGrid->addChild<ButtonModel>("5", IGridLayout::GridPos{75, 150, 75, 75});
	auto b_six = buttonGrid->addChild<ButtonModel>("6", IGridLayout::GridPos{150, 150, 75, 75});
	auto b_sub = buttonGrid->addChild<ButtonModel>("sub", IGridLayout::GridPos{225, 150, 75, 75});

	/* Row 4 */
	auto b_one = buttonGrid->addChild<ButtonModel>("1", IGridLayout::GridPos{0, 225, 75, 75});
	auto b_two = buttonGrid->addChild<ButtonModel>("2", IGridLayout::GridPos{75, 225, 75, 75});
	auto b_three = buttonGrid->addChild<ButtonModel>("3", IGridLayout::GridPos{150, 225, 75, 75});
	auto b_plus = buttonGrid->addChild<ButtonModel>("plus", IGridLayout::GridPos{225, 225, 75, 75});

	/* Row 5 */
	auto b_zero = buttonGrid->addChild<ButtonModel>("0", IGridLayout::GridPos{75, 300, 75, 75});
	auto b_result = buttonGrid->addChild<ButtonModel>("result", IGridLayout::GridPos{225, 300, 75, 75});

	// 1
	b_one->guiBaseOptions.width = 75;
	b_one->guiBaseOptions.height = 75;
	b_one->text = "1";
	b_one->callback = [&](){
		result->text = engine.insertNumber(1.0f);
		viewContainer.updateWidget(result->id);
	};

	// 2
	b_two->guiBaseOptions.width = 75;
	b_two->guiBaseOptions.height = 75;
	b_two->text = "2";
	b_two->callback = [&](){
		result->text = engine.insertNumber(2.0f);
		viewContainer.updateWidget(result->id);
	};

	// 3
	b_three->guiBaseOptions.width = 75;
	b_three->guiBaseOptions.height = 75;
	b_three->text = "3";
	b_three->callback = [&](){
		result->text = engine.insertNumber(3.0f);
		viewContainer.updateWidget(result->id);
	};

	// 4
	b_four->guiBaseOptions.width = 75;
	b_four->guiBaseOptions.height = 75;
	b_four->text = "4";
	b_four->callback = [&](){
		result->text = engine.insertNumber(4.0f);
		viewContainer.updateWidget(result->id);
	};

	// 5
	b_five->guiBaseOptions.width = 75;
	b_five->guiBaseOptions.height = 75;
	b_five->text = "5";
	b_five->callback = [&](){
		result->text = engine.insertNumber(5.0f);
		viewContainer.updateWidget(result->id);
	};

	// 6
	b_six->guiBaseOptions.width = 75;
	b_six->guiBaseOptions.height = 75;
	b_six->text = "6";
	b_six->callback = [&](){
		result->text = engine.insertNumber(6.0f);
		viewContainer.updateWidget(result->id);
	};

	// 7
	b_seven->guiBaseOptions.width = 75;
	b_seven->guiBaseOptions.height = 75;
	b_seven->text = "7";
	b_seven->callback = [&](){
		result->text = engine.insertNumber(7.0f);
		viewContainer.updateWidget(result->id);
	};

	// 8
	b_eight->guiBaseOptions.width = 75;
	b_eight->guiBaseOptions.height = 75;
	b_eight->text = "8";
	b_eight->callback = [&](){
		result->text = engine.insertNumber(8.0f);
		viewContainer.updateWidget(result->id);
	};

	// 9
	b_nine->guiBaseOptions.width = 75;
	b_nine->guiBaseOptions.height = 75;
	b_nine->text = "9";
	b_nine->callback = [&](){
		result->text = engine.insertNumber(9.0f);
		viewContainer.updateWidget(result->id);
	};

	// 0
	b_zero->guiBaseOptions.width = 75;
	b_zero->guiBaseOptions.height = 75;
	b_zero->text = "0";
	b_zero->callback = [&](){
		result->text = engine.insertNumber(0.0f);
		viewContainer.updateWidget(result->id);
	};

	// +
	b_plus->guiBaseOptions.width = 75;
	b_plus->guiBaseOptions.height = 75;
	b_plus->text = "+";
	b_plus->callback = [&] {
		result->text = engine.add();
		viewContainer.updateWidget(result->id);
	};

	// -
	b_sub->guiBaseOptions.width = 75;
	b_sub->guiBaseOptions.height = 75;
	b_sub->text = "-";
	b_sub->callback = [&] {
		result->text = engine.subtract();
		viewContainer.updateWidget(result->id);
	};

	// *
	b_mul->guiBaseOptions.width = 75;
	b_mul->guiBaseOptions.height = 75;
	b_mul->text = "x";
	b_mul->callback = [&] {
		result->text = engine.multiply();
		viewContainer.updateWidget(result->id);
	};

	// /
	b_div->guiBaseOptions.width = 75;
	b_div->guiBaseOptions.height = 75;
	b_div->text = "/";
	b_div->callback = [&] {
		result->text = engine.divide();
		viewContainer.updateWidget(result->id);
	};

	// Button result
	b_result->guiBaseOptions.width = 75;
	b_result->guiBaseOptions.height = 75;
	b_result->text = "=";
	b_result->callback = [&] {
		result->text = engine.getResult();
		viewContainer.updateWidget(result->id);
	};

	// Clear
	b_clear->guiBaseOptions.width = 75;
	b_clear->guiBaseOptions.height = 75;
	b_clear->text = "C";
	b_clear->callback = [&] {
		result->text = engine.clear();
		viewContainer.updateWidget(result->id);
	};

	viewContainer.updateWidget(*listLayoutModel);
	viewContainer.startView();
	viewContainer.endView();

	return 0;
}
