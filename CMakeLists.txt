cmake_minimum_required(VERSION 3.10)
project(Calculator LANGUAGES CXX)

link_directories(${CMAKE_CURRENT_SOURCE_DIR}/sdk/libs)

add_executable(Calculator src/main.cpp src/Engine.cpp src/Engine.hh)

target_compile_options( ${PROJECT_NAME} PRIVATE -W -Wall -Wextra)
target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_14)

find_package( PkgConfig )
pkg_check_modules( GTKMM gtkmm-3.0 )

#Suppress warnings for external lib
target_include_directories(${PROJECT_NAME}
        PRIVATE
            src
        SYSTEM
        PUBLIC
        ${GTKMM_INCLUDE_DIRS}
        )

target_compile_options( ${PROJECT_NAME} PRIVATE -W -Wall -Wextra)
target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_14)


target_link_libraries(${PROJECT_NAME} ${GTKMM_LIBRARIES} )
target_link_libraries(${PROJECT_NAME} "MVC")
#target_link_libraries(${PROJECT_NAME} "TTS")
target_link_libraries(${PROJECT_NAME} "GUI")


target_include_directories(${PROJECT_NAME}
        PRIVATE
            "sdk/includes/"
        )